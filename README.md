# Backport for RHEL6

This repo contains the scripts and CI jobs for building newer versions of
different pieces of software on **RHEL6**, to be used by the RHEL6 builder
container image at **CKI**.

## Software backported

Currently the following software are being compiled on RHEL6:

* **Bash 4.4**
* **OpenSSL 1.0.2u**
* **Python 3.7.0**

## How is it used?

The software compiled here will be stored as artifacts to be used at
build time for the [builder-rhel6] template at the [CKI containers repository].

Using the feature [needs] from Gitlab CI, an artifact from this project can be
used from another project.

> NOTE: Using artifacts between projects is a premium feature. CKI uses it
> because is part of the [Gitlab Open Source program].

Here is an example of how to use it from another project:

```yaml
builder-rhel6:
  extends: [.publish_local, .internal_regs]
  needs:
    - project: cki-project/backports/rhel6
      job: build-python
      ref: main
      artifacts: true
    - project: cki-project/backports/rhel6
      job: build-bash
      ref: main
      artifacts: true
```

[builder-rhel6]: https://gitlab.com/cki-project/containers/-/blob/main/builds/builder-rhel6.in
[CKI containers repository]: https://gitlab.com/cki-project/containers/
[needs]: https://docs.gitlab.com/ee/ci/yaml/README.html#cross-project-artifact-downloads-with-needs
[Gitlab Open Source program]: https://about.gitlab.com/solutions/open-source/

## Maintainers

* Juanje Ojeda ([juanjeojeda](https://gitlab.com/juanjeojeda))
